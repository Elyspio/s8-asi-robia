# UTP / TCP

Dans cette partie nous restons dans les technologies bas niveaux avec l'utilisation directe du protocole UDP et TCP en
python

## Scénario

<img src="https://gitlab.com/Elyspio/s8-asi-robia/-/raw/master/diagram/img/scenario-raw.png" height=700></img>
Dans cette partie, nous allons illustrer comment interagirait un ensemble de capteurs (thermomètres) avec un serveur "
controller" et des actuateurs (radiateurs)

Dans notre scénario, les radiateurs seront activés en fonction de la temperature de l'appartement

## UDP

### Client

````python
import socket
import time

server_ip = '127.0.0.1'  # Server IP address, use the command `ip a` on the server to get it
# or use '127.0.0.1' if both the server and client are on the same machine
port = 4299  # Listening port on the server

data = "Some useful data"  # Data to send to the server

client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)  # Create the UDP client socket

print("UDP client ready to send to", server_ip, "on port", port)

while True:
    client_socket.sendto(data.encode('UTF-8'), (server_ip, port))  # Send the data to the server via UDP
    print("Data sent to", server_ip)
    time.sleep(.5)


````

### Serveur

````python
import socket

local_ip = '0.0.0.0'  # IP address to listen on: can be a specific IP to listen on a specific interface
# or '0.0.0.0' to listen on every interface
port = 4299  # Server port, can be any port from 1024 to 49151
buffer_size = 1024  # Buffer length, can receive

server_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)  # Create the UDP server socket
server_socket.bind((local_ip, port))  # Start a UDP server

print("UDP server listening from", local_ip, "on port", port)

while True:
    data, address = server_socket.recvfrom(buffer_size)  # Wait for a client to receive some data
    print('Received: "', data.decode('UTF-8'), '" from ', address[0], sep='')

    # Do some stuff...

````

## TCP (basic : point à point)

### Client

````python
import socket
import time

server_ip = '127.0.0.1'  # Server IP address, use the command `ip a` on the server to get it
# or use '127.0.0.1' if both the server and client are on the same machine
port = 4299  # Listening port on the server

data = "Some useful data"  # Data to send to the server

client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)  # Create the TCP client socket
client_socket.connect((server_ip, port))

print("TCP client ready to send to", server_ip, "on port", port)

try:
    while True:
        client_socket.sendall(data.encode('UTF-8'))  # Send the data to the server via TCP
        print("Data sent to", server_ip)

        # Do some stuff...

        time.sleep(.5)
finally:
    client_socket.close()

````

### Serveur

````python
import socket

local_ip = '0.0.0.0'  # IP address to listen on: can be a specific IP to listen on a specific interface
# or '0.0.0.0' to listen on every interface
port = 4299  # Server port, can be any port from 1024 to 49151
buffer_size = 1024  # Buffer length, can't receive

server_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)  # Create the UDP server socket
server_socket.bind((local_ip, port))  # Start a TCP server
server_socket.listen()  # Listen for incoming connections

print("TCP server listening from", local_ip, "on port", port)

while True:
    connection, address = server_socket.accept()  # Wait for a client to receive some data
    print("Connection opened from", address[0], "on port", address[1])

    try:
        while True:
            data = connection.recv(buffer_size)
            if data:
                print('Received: "', data.decode('UTF-8'), '"', sep='')
                # Do some stuff...
            else:  # No data received, stop listening
                break
    finally:
        connection.close()  # Close the connection


````

## TCP (multi-client)

Un exemple complet répondant au scénario (plusieurs thermomètres, radiateurs communiquant avec un controleur) est
disponible [ici](simple-scenario)




