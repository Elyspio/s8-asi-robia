import multiprocessing as mp
import socket
import time

sensors_listen_ip = '0.0.0.0'
sensors_listen_port = 4201  # UDP

radiators_listen_ip = '0.0.0.0'  # Negociation phase
radiators_listen_port = 4202  # TCP

radiators_port_range = (14200, 14299)  # Port range for radiator connections after negociation phase


def main():
    print("Hub starting")

    manager = mp.Manager()
    sensors_data = manager.dict()

    radiators_negoc_process = mp.Process(target=radiators_negoc_listener, args=(sensors_data,))
    radiators_negoc_process.start()

    sensors_process = mp.Process(target=sensors_listener, args=(sensors_data,))
    sensors_process.start()

    print("Hub ready")

    while True:
        time.sleep(1)


def sensors_listener(sensors_data):
    sensors_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    sensors_socket.bind((sensors_listen_ip, sensors_listen_port))

    while True:
        data, address = sensors_socket.recvfrom(64)
        data = int(data.decode('UTF-8'))
        sensors_data[address] = data
        print("Received ", data, " from ", address[0], ":", address[1], sep="")


def radiators_negoc_listener(sensors_data):
    radiators_negoc_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    radiators_negoc_socket.bind((radiators_listen_ip, radiators_listen_port))
    radiators_negoc_socket.listen()

    next_port = radiators_port_range[0]

    while True:
        connection, address = radiators_negoc_socket.accept()
        data = connection.recv(128).decode('UTF-8')

        if data.startswith("CONNECT RADIATOR"):
            print("Connection request from", address[0])

            radiator_direct_connection = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
            connected = False

            while not connected:
                try:
                    radiator_direct_connection.bind((radiators_listen_ip, next_port))
                    radiator_direct_connection.listen()
                    connected = True
                    current_port = next_port
                except OSError:
                    pass

                next_port += 1
                if next_port > radiators_port_range[1]:
                    next_port = radiators_port_range[0]

            # socket_ready = mp.Semaphore(0)
            mp.Process(target=radiator_connection, args=(radiator_direct_connection, sensors_data)).start()

            # socket_ready.acquire()
            # print("Direct socket ready")
            connection.send(str(current_port).encode('UTF-8'))
            connection.close()


def radiator_connection(socket, sensors_data):
    # socket_ready.release()
    connection, address = socket.accept()
    print("Radiator ", address[0], ":", address[1], " connected", sep="")

    while True:
        mean_temp = sum(sensors_data.values()) / len(sensors_data)
        radiator_power = 0

        if mean_temp < 10:
            radiator_power = 100
        elif mean_temp < 20:
            radiator_power = 50
        else:
            radiator_power = 0

        data = str(radiator_power).encode('UTF-8')
        connection.send(data)

        time.sleep(2)


if __name__ == '__main__':
    main()
