# import json
import random
import socket
import time

server_ip = '127.0.0.1'  # Server IP address, use the command `ip a` on the server to get it
# or use '127.0.0.1' if both the server and client are on the same machine
port = 4201  # Listening port on the server

data = 20

client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)  # Create the UDP client socket

print("UDP client ready to send to", server_ip, "on port", port)

while True:
    data += random.randint(-2, 2)
    print("Current temperature:", data)

    # client_socket.sendto(json.dumps(data).encode('UTF-8'), (server_ip, port)) # Send the data to the server via UDP
    client_socket.sendto(str(data).encode('UTF-8'), (server_ip, port))  # Send the data to the server via UDP
    # print("Data sent to", server_ip)

    time.sleep(1)
