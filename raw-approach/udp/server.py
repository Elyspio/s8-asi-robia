import socket

local_ip = '0.0.0.0'  # IP address to listen on: can be a specific IP to listen on a specific interface
# or '0.0.0.0' to listen on every interface
port = 4299  # Server port, can be any port from 1024 to 49151
buffer_size = 1024  # Buffer length, can't receive

server_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)  # Create the UDP server socket
server_socket.bind((local_ip, port))  # Start a UDP server

print("UDP server listening from", local_ip, "on port", port)

while True:
    data, address = server_socket.recvfrom(buffer_size)  # Wait for a client to receive some data
    print('Received: "', data.decode('UTF-8'), '" from ', address[0], sep='')

    # Do some stuff...
