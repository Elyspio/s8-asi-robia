import json
import multiprocessing as mp
import socket
import time

import cv2
import numpy

sensors_listen_ip = '0.0.0.0'
sensors_listen_port = 4201  # UDP
sensors_data_key = "TEMP"

camera_listen_ip = '0.0.0.0'
camera_listen_port = 4203  # TCP
camera_data_key = "CAMERA"

radiators_listen_ip = '0.0.0.0'  # Negociation phase
radiators_listen_port = 4202  # TCP
radiators_port_range = (14200, 14299)  # Port range for radiator connections after negociation phase

visualizers_listen_ip = '0.0.0.0'  # Negociation phase
visualizers_listen_port = 4204  # TCP
visualizers_port_range = (14300, 14399)  # Port range for visualizer connections after negociation phase


def main():
    print("Hub starting")

    manager = mp.Manager()
    sensors_data = manager.dict()

    sensors_process = mp.Process(target=sensors_listener, args=(sensors_data,))
    sensors_process.start()

    cameras_process = mp.Process(target=cameras_listener, args=(sensors_data,))
    cameras_process.start()

    radiators_negoc_process = mp.Process(target=radiators_negoc_listener, args=(sensors_data,))
    radiators_negoc_process.start()

    visualizers_negoc_process = mp.Process(target=visualizers_negoc_listener, args=(sensors_data,))
    visualizers_negoc_process.start()

    print("Hub ready")

    while True:
        time.sleep(1)


def sensors_listener(sensors_data):
    sensors_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    sensors_socket.bind((sensors_listen_ip, sensors_listen_port))

    while True:
        data, address = sensors_socket.recvfrom(64)
        data = int(data.decode('UTF-8'))
        sensors_data[sensors_data_key][address] = data
        print("Received ", data, " from ", address[0], ":", address[1], sep="")


def cameras_listener(sensors_data):
    cameras_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    cameras_socket.bind((camera_listen_ip, camera_listen_port))
    cameras_socket.listen()

    while True:
        connection, _ = cameras_socket.accept()
        try:
            data = connection.recv(128).decode('UTF-8')
            data = json.loads(data)

            img = bytearray()

            for _ in range(data["nb_packets"]):
                img.append(connection.recv(1024))

            print(img)
            img = numpy.frombuffer(img, dtype=numpy.uint8).reshape(1, -1)
            img = cv2.imdecode(img, cv2.IMREAD_COLOR)
            sensors_data[camera_data_key][data["room"]]

        finally:
            connection.close()


def radiators_negoc_listener(sensors_data):
    radiators_negoc_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    radiators_negoc_socket.bind((radiators_listen_ip, radiators_listen_port))
    radiators_negoc_socket.listen()

    next_port = radiators_port_range[0]

    while True:
        connection, address = radiators_negoc_socket.accept()
        data = connection.recv(128).decode('UTF-8')

        if data.startswith("CONNECT RADIATOR"):
            print("Radiator connection request from", address[0])

            radiator_direct_connection = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
            connected = False

            while not connected:
                try:
                    radiator_direct_connection.bind((radiators_listen_ip, next_port))
                    radiator_direct_connection.listen()
                    connected = True
                    current_port = next_port
                except OSError:
                    pass

                next_port += 1
                if next_port > radiators_port_range[1]:
                    next_port = radiators_port_range[0]

            mp.Process(target=radiator_connection, args=(radiator_direct_connection, sensors_data)).start()

            connection.send(str(current_port).encode('UTF-8'))
            connection.close()


def radiator_connection(socket, sensors_data):
    connection, address = socket.accept()
    print("Radiator ", address[0], ":", address[1], " connected", sep="")

    while True:
        mean_temp = sum(sensors_data.values()) / len(sensors_data)
        radiator_power = 0

        if mean_temp < 10:
            radiator_power = 100
        elif mean_temp < 20:
            radiator_power = 50
        else:
            radiator_power = 0

        data = str(radiator_power).encode('UTF-8')
        connection.send(data)

        time.sleep(2)


def visualizers_negoc_listener(sensors_data):
    visualizers_negoc_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
    visualizers_negoc_socket.bind((visualizers_listen_ip, visualizers_listen_port))
    visualizers_negoc_socket.listen()

    next_port = visualizers_port_range[0]

    while True:
        connection, address = visualizers_negoc_socket.accept()
        data = connection.recv(128).decode('UTF-8')

        if data.startswith("CONNECT VISUALIZER"):
            print("Visualizer connection request from", address[0])

            visualizer_direct_connection = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
            connected = False

            while not connected:
                try:
                    visualizer_direct_connection.bind((visualizers_listen_ip, next_port))
                    visualizer_direct_connection.listen()
                    connected = True
                    current_port = next_port
                except OSError:
                    pass

                next_port += 1
                if next_port > visualizers_port_range[1]:
                    next_port = visualizers_port_range[0]

            mp.Process(target=visualizer_connection, args=(visualizer_direct_connection, sensors_data)).start()

            connection.send(str(current_port).encode('UTF-8'))
            connection.close()


def visualizer_connection(socket, sensors_data):
    connection, address = socket.accept()
    print("Visualizer ", address[0], ":", address[1], " connected", sep="")

    while True:
        mean_temp = sum(sensors_data.values()) / len(sensors_data)

        data = str(visualizer_power).encode('UTF-8')
        connection.send(data)

        time.sleep(2)


if __name__ == '__main__':
    main()
