import socket

server_ip = '127.0.0.1'  # Server IP address, use the command `ip a` on the server to get it
# or use '127.0.0.1' if both the server and client are on the same machine
port = 4202  # Listening port on the server

connection_string = "CONNECT RADIATOR"

client_negoc_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)  # Create the TCP client socket

try:
    client_negoc_socket.connect((server_ip, port))
except:
    print("Error while connecting to the server")
    exit(-1)

client_negoc_socket.send(connection_string.encode('UTF-8'))
data = client_negoc_socket.recv(64).decode('UTF-8')
client_negoc_socket.close()

direct_port = int(data)

client_direct_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
client_direct_socket.connect((server_ip, direct_port))

while True:
    data = client_direct_socket.recv(64).decode('UTF-8')
    print("Radiator power:", data)
