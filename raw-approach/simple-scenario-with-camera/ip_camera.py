import json
import socket

import cv2

# import time


server_ip = '127.0.0.1'  # Server IP address, use the command `ip a` on the server to get it
# or use '127.0.0.1' if both the server and client are on the same machine
port = 4203  # Listening port on the server

buffer_size = 1024

camera = cv2.VideoCapture(0)

print("Camera ready")

while True:
    success, img = camera.read()
    if success:
        try:
            print("Sending")
            img = cv2.imencode('.jpg', img)[1].tobytes()
            nb_packets = ((len(img) - 1) // buffer_size) + 1

            data = {"nb_packets": nb_packets, "room": "test"}
            data = json.dumps(data)

            client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)  # Create the TCP client socket
            client_socket.connect((server_ip, port))
            client_socket.send(data.encode('UTF-8'))

            for i in range(nb_packets):
                client_socket.sendto(img[buffer_size * nb_packets:buffer_size * (nb_packets + 1)], (server_ip, port))  # Send the data to the server via UDP

        finally:
            client_socket.close()
