import socket

local_ip = '0.0.0.0'  # IP address to listen on: can be a specific IP to listen on a specific interface
# or '0.0.0.0' to listen on every interface
port = 4299  # Server port, can be any port from 1024 to 49151
buffer_size = 1024  # Buffer length, can't receive

server_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)  # Create the UDP server socket
server_socket.bind((local_ip, port))  # Start a TCP server
server_socket.listen()  # Listen for incoming connections

print("TCP server listening from", local_ip, "on port", port)

while True:
    connection, address = server_socket.accept()  # Wait for a client to receive some data
    print("Connection opened from", address[0], "on port", address[1])

    try:
        while True:
            data = connection.recv(buffer_size)
            if data:
                print('Received: "', data.decode('UTF-8'), '"', sep='')
                # Do some stuff...
            else:  # No data received, stop listening
                break
    finally:
        connection.close()  # Close the connection
