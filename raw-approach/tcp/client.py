import socket
import time

server_ip = '127.0.0.1'  # Server IP address, use the command `ip a` on the server to get it
# or use '127.0.0.1' if both the server and client are on the same machine
port = 4299  # Listening port on the server

data = "Some useful data"  # Data to send to the server

client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)  # Create the TCP client socket
client_socket.connect((server_ip, port))

print("TCP client ready to send to", server_ip, "on port", port)

try:
    while True:
        client_socket.sendall(data.encode('UTF-8'))  # Send the data to the server via TCP
        print("Data sent to", server_ip)

        # Do some stuff...

        time.sleep(.5)
finally:
    client_socket.close()
