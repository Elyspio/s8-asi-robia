from diagrams import Cluster, Diagram
from diagrams.aws.compute import Compute
from diagrams.aws.iot import IotAction, IotActuator, IotCamera, IotSitewise, IotThermostat


def get_room(x: str):
    with Cluster(x):
        db_main = [IotThermostat(f"Thermostat {x}"), IotCamera(f"Camera {x}")]
        return db_main


def get_heat_sink():
    with Cluster("Radiators"):
        db_main = list(map(lambda x: IotActuator(f"Radiator {x}"), range(0, 3)))
        return db_main


def get_controller():
    return Compute("Controller")


if __name__ == '__main__':
    with Diagram("img/scenario", show=True, direction="LR"):
        rooms = [*get_room("Chamber"), *get_room("Kitchen")]
        controller = get_controller()

        rooms >> controller >> [IotAction("Enrich camera info")] >> IotSitewise("Front")
