# Web

Afin de montrer la possibilité d'utiliser les technologies du web pour s'échanger des informations en python : nous
avons choisi de mettre en place le micro-framework `flask` avec sa surcouche `flask restx`

## Scénario

<img src="https://gitlab.com/Elyspio/s8-asi-robia/-/raw/master/diagram/img/scenario-web.png" height=700></img>

Dans cette partie, nous allons illustrer comment interagirait un ensemble de capteurs : thermomètres, caméras avec un
serveur "controller" et un système de visualisation des données (front).

Dans notre scénario, nous avons un ensemble caméra/thermomètre dans deux pièces de l'appartement (dans notre cas : la
cuisine et une chambre). On affichera pour chaque chambre, l'image issues de la caméra avec écrit en surimpression la
température de la pièce.

## Flask

Flask est un micro-service dans le sens qu'il n'impose pas une structure du code particulière dans arborescence des
fichiers ou de la manière d'intéraction avec la base de données comme le ferait le framework python Django.

Il permet de réaliser des Api REST full de façon simplifiée et intuitive :

```python
# Controlleur REST proposant des points d'acces REST et POST

from flask import Flask, render_template
from flask_cors import CORS
from flask_restx import Api

app = Flask(__name__)

CORS(app)  # Autorise le serveur à être appelé depuis des pages ayant une autre origine que lui-même 


@app.route("/")
def fonction_appelee_lors_une_requète_get_vers_la_route_ci_dessus():
    return str("Réponse à un appel GET")


@app.route("/", methods=["POST"])
def fonction_appelee_lors_une_requète_post_vers_la_route_ci_dessus():
    return str("Réponse à un appel POST")


```

La librairie `flask-restx` nous permet une approche basée sur des classes

```python
from flask_restx import Resource, fields, Api

api = Api(app, version='1.0', title='Hub API', description='A simple hub API', )

ns_temp = api.namespace('Index', description='Index operations')

model = api.model("post", {
    "value": fields.Float
}),


@ns_temp.route('/')
class Index(Resource):

    def get(self):
        # Sera appelée lors d'un appel HTTP GET sur la route /
        pass

    @ns_temp.expect(model)
    def post(self):
        # Sera appelée lors d'un appel HTTP POST sur la route /
        # le décorateur ns_temp.expect permet de préciser les paramètres à passer lors de l'appel
        # ici une valeur flottante pour le champ "value" doit être passée en body de la requète
        pass


```

Cette librairie nous permet avec ses annotations (`expect`, etc.) de produire une documentation swagger à partir du
code.



#### Envoi d'un flux de données

Contrairement à l'envoie de données "simple" (text) qui est assez trivial, la gestion des flux est plus compliquées.

Dans ce scénario, afin de récupérer l'image des caméras, cette dernière envoie pour chaque frame (image) une requète
POST en direction du controlleur.

````python
# Exemple d'envoi d'une requète POST
import requests
import os

room = os.getenv("ROOM",
                 "unspecified")  # récupération de la pièce ou se situe la cémaré (configation utilisée pour le déploiement)
hud_host = os.getenv("HUD_HOST", "http://127.0.0.1:5000")  # récupération de l'adresse du controlleur

requests.post(f"{hud_host}/sensors/cameras/{room}",
              data=xxx)  # Envoie d'une requète post vers la route `/sensors/cameras/{room}` du controlleur 

````

> Attention pour des raisons de performances il vaut mieux privilégier `127.0.0.1` à `localhost`

#### Envoi d'un flux mjpeg

La librairie `flask` nous permet de gerer facilement les streams de données avec la classe `Response` en lui passant une fonction génératrice (voir exemple ci-dessous)

````python
from flask import Response, request


@ns_temp.route('/cameras/<room>')
class Camera(Resource):

    def get(self, room):
        raw = request.args.get("raw")
        return Response(get_data(room, raw is not None), mimetype='multipart/x-mixed-replace; boundary=frame')


data = None # see app/data.py for more information

def get_data(room, raw=False):
    while True:
        frame = [data[room].video.processed_stream, data[room].video.raw_stream][raw]
        frame = (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # Création d'une image jpeg à partir de son binaire
        yield frame


````


### Swagger

Un swagger est une documentation explicite d'une API permettant de tester les points d'entrée depuis un navigateur WEB (swagger-ui) mais aussi de pouvoir générer du code "client" appelant ces derniers.

Le swagger ci-dessous représente l'API imaginé pour répondre au scénario.

![Swagger](../diagram/img/web-approach-swagger.png)



## OpenCV


### Récupération des images de la caméra

Afin de pouvoir récupérer l'image de la caméra, nous utilisons la librarie OpenCV :

`sudo apt install python3-opencv && sudo pip3 install opencv-python`

Création d'une classe python permettant de récupérer une frame de la vidéo

````python
import cv2


class VideoCamera(object):
    def __init__(self, cam_index: int):
        # capturing video
        self.video = cv2.VideoCapture(cam_index)

    def __del__(self):
        # releasing camera
        self.video.release()

    def get_frame(self):
        # extracting frames
        ret, frame = self.video.read()
        ret, img = cv2.imencode('.jpg', frame)

        return img.tobytes()


camera = VideoCamera(0)  # Instantiation de la caméra indexé "0"

camera.get_frame()  # Renvera l'image "courante" en tant que tableau de bytes

````

### Modification des images

Dans le scénario on souhaite imprimer en sur-impression la température de la pièce au flux de la caméra.

OpenCV nous le permet avec la fonction `putText`



````python
import numpy as np
import cv2
font = cv2.FONT_HERSHEY_SIMPLEX

def handle_new_img(frame: bytes):
    jpg_as_np = np.frombuffer(frame, dtype=np.uint8) # Passage d'un tableau en tableau "numpyesc"
    img = cv2.imdecode(jpg_as_np, flags=cv2.IMREAD_COLOR) # Convertion du tableau binaire en image "opencv"

    temp = 5 # Ici on fixe 5°C pour l'exemple
    img = cv2.putText(img, f'Temp: {temp}', (5, 50), font, 1, (255, 0, 0), 2) # Ajout du texte dans l'image
    return cv2.imencode(".jpg", img)[1].tobytes() # Re conversion de l'image en bytes


raw_image = camera.get_frame()
processed_image = handle_new_img(raw_image)

````




## Docker 

La configuration des scripts python (caméra à utiliser, chambre où est installée le capteur) se fait par les variables d'environements

Nous utilisons l'image de base de python + installation des dépendences à partir des `requirements.txt` pour chaque scripts.

Les serveurs flask (hud et actuateurs) ne sont pas des serveurs de production.

Une alternative à flask aurait pu être d'utiliser la librairie [fastApi](https://fastapi.tiangolo.com/) qui gère la documentation Swagger de manière bien plus naturelle et qui est bien plus performante 


### Dockerfiles:
- [hud](hub/hub.dockerfile)
- [camera](sensors/camera/camera.dockerfile)
- [thermometre](sensors/thermometer/thermometer.dockerfile)

### Docker-Compose

Le fichier docker-compose.yml créant le scénario (avec les deux caméras sur la machine de l'hôte) se trouve [ici](scenario.docker-compose.yml) 

> Attention Docker pour Windows ne gère pas les caméras, il faut absolument être sous linux ou sous une VM linux



## Rendu


Un exemple complet répondant au scénario est disponible dans le dossier courant
- hud : controleur et front 
- actuators : radiateurs
- sensors : thermomètres et caméras

Ci-dessous une capture d'écran montrant le résutat final :

<img src="https://gitlab.com/Elyspio/s8-asi-robia/-/raw/master/diagram/img/web-approach-working-example.png" height=700></img>
