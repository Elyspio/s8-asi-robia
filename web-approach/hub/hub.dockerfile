FROM python


RUN apt-get update && apt-get install -y python3-opencv


COPY requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt



COPY ./ /app

ENV FLASK_APP main.py

EXPOSE 80
WORKDIR /app
CMD ["python3",  "-m",  "flask", "run", "--host=0.0.0.0" , "--port=80"]