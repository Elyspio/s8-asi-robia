from flask_restx import Resource, fields

from .data import data
from .hub import api

models_data = {
    "post": api.model("data-float", {
        "value": fields.Float
    }),
}

ns_temp = api.namespace('sensors', description='Data operations')


@ns_temp.route('/thermometers/<room>')
class Camera(Resource):

    @ns_temp.expect(models_data["post"])
    def post(self, room):
        value = api.payload["value"]
        data[room].temperature = value


name = "Temperature"
