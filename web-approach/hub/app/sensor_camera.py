import cv2
import numpy as np
from flask import Response, request
from flask_restx import Resource, fields

from .data import Data, data, get_data
from .hub import api

font = cv2.FONT_HERSHEY_SIMPLEX

models_data = {
    "get_temperature": api.model("data-int", {
        "average": fields.Float,
        "nb_val": fields.Integer
    }),
    "post_temperature": api.model("data-float", {
        'room': fields.String,
        "value": fields.Float
    }),
}

ns_temp = api.namespace('sensors', description='Sensor: Camera operations')


@ns_temp.route('/cameras/<room>')
class Camera(Resource):

    @ns_temp.expect(models_data["post_temperature"])
    def post(self, room):
        img_bytes = request.data
        if room not in data:
            data[room] = Data()

        data[room].video.processed_stream = self.handle_new_img(room, img_bytes)
        data[room].video.raw_stream = img_bytes


    def handle_new_img(self, room, frame):
        jpg_as_np = np.frombuffer(frame, dtype=np.uint8)
        img = cv2.imdecode(jpg_as_np, flags=cv2.IMREAD_COLOR)

        temp = data[room].temperature
        img = cv2.putText(img, f'Temp: {temp}', (5, 50), font, 1, (255, 0, 0), 2)
        return cv2.imencode(".jpg", img)[1].tobytes()

    def get(self, room):
        raw = request.args.get("raw")
        return Response(get_data(room, raw is not None), mimetype='multipart/x-mixed-replace; boundary=frame')


name = "Camera"
