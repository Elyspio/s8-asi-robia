class VideoStream:
    raw_stream: bytes
    processed_stream: bytes


class Data:
    temperature: float
    video: VideoStream

    def __init__(self):
        self.temperature = 0
        self.video = VideoStream()


data = {
    "_": Data()
}


def get_data(room, raw=False):
    while True:
        frame = [data[room].video.processed_stream, data[room].video.raw_stream][raw]
        frame = (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result
        yield frame
