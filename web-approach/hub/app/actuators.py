from flask import jsonify, request
from flask_restx import Resource, fields

from .hub import api

models_data = {
    "register": api.model("data-int", {
        "type": fields.String,
        "url": fields.String
    }),
}

actuators = {}

ns_temp = api.namespace('actuators', description='Data operations')


@ns_temp.route('/register')
class ActuatorRegister(Resource):

    @ns_temp.expect(models_data["register"])
    def post(self):
        print(f"{request.remote_addr} is registering as {api.payload['type']}")
        type = api.payload["type"]
        addr = api.payload["url"]
        if type not in actuators:
            actuators[type] = [addr]
        else:
            if addr not in actuators[type]:
                actuators[type].append(addr)


    def get(self):
        return jsonify(actuators)


name = "Actuator"
