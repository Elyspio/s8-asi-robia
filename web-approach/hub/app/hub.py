# Fix flask_restx

import flask.scaffold
from flask import Flask, render_template
from flask_cors import CORS

flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func

from flask_restx import Api

app = Flask(__name__)
api = Api(app, version='1.0', title='Hub API', description='A simple hub API', )

from .sensor_temperature import name as temperature_server_name
from .sensor_camera import name as camera_server_name
from .actuators import name as actuators_server_name

CORS(app)


@app.route("/front")
def index():
    return render_template("index.html")


def run(debug, port):
    for name in [temperature_server_name, camera_server_name, actuators_server_name]:
        print(f"loading server {name}")

    app.run(host="0.0.0.0", debug=debug, threaded=True, port=port)


if __name__ == '__main__':
    run(True, 8000)
