from requests import request


def register(core_url: str, component_name: str, component_url: str):
    print(f"try to register to {core_url} as {component_name}")
    url = f"{core_url}/actuators/register"
    request("POST", url, json={"type": component_name, "url": component_url})
