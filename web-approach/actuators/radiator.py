import os
from os import environ

import flask.scaffold
from flask import Flask
from flask_cors import CORS

flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func

from flask_restx import Api, Resource
from .utils import register

component_name = "Radiator"

app = Flask(__name__)
api = Api(app, version='1.0', title=f'{component_name} API', description=f'A simple {component_name} API', )

from flask_restx import fields

models_data = {
    "post_temperature_alter": api.model("post_temperature_alter", {
        "factor": fields.Float
    }),
    "data-temperature": api.model("data-temperature", {
        "temperature": fields.Float
    }),

}

HUB_URL = "http://127.0.0.1:5000" or os.getenv("HUB_URL")

ns_temp = api.namespace('radiator', description='Radiator')

data = {
    'factor': 1
}


@ns_temp.route('/temperature/reduce')
class RadiatorTemperatureReduce(Resource):

    @ns_temp.expect(models_data["post_temperature_alter"])
    @ns_temp.marshal_with(models_data["data-temperature"])
    def post(self):
        data["factor"] += 1 * api.payload["factor"]

        pass


@ns_temp.route('/temperature/increase')
class RadiatorTemperatureIncrease(Resource):

    @ns_temp.expect(models_data["post_temperature_alter"])
    @ns_temp.marshal_with(models_data["data-temperature"])
    def post(self):
        data["factor"] -= 1 * api.payload["factor"]
        pass


CORS(app)

register(HUB_URL, component_name, f"{environ.get('HTTP_HOST')}")

if __name__ == '__main__':
    print(f"loading server Radiator")
    app.run(debug=True, port=environ.get("HTTP_PORT"))
