FROM python

RUN mkdir -p /app
WORKDIR /app

RUN apt-get update && apt-get install -y python3-opencv


COPY camera.requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt

COPY camera.py /app/camera.py


CMD ["python3", "/app/camera.py"]