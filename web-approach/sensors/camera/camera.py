import os
import time

import cv2
import requests


class VideoCamera(object):
    def __init__(self, cam_index: int):
        # capturing video
        self.video = cv2.VideoCapture(cam_index)

    def __del__(self):
        # releasing camera
        self.video.release()

    def get_frame(self):
        # extracting frames
        ret, frame = self.video.read()
        ret, img = cv2.imencode('.jpg', frame)

        return img.tobytes()


cam_id = int(os.getenv("CAM", "0"))
room = os.getenv("ROOM", "unspecified")
hud_host = os.getenv("HUD_HOST", "http://127.0.0.1:5000")


camera = VideoCamera(cam_id)

session = requests.Session()

if __name__ == '__main__':
    while True:
        try:
            session.post(f"{hud_host}/sensors/cameras/{room}", data=camera.get_frame())
            time.sleep(1 / 60)

        except Exception as e:
            print("error", e)
            pass
