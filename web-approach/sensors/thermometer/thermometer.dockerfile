FROM python

RUN mkdir -p /app
WORKDIR /app

COPY thermometer.requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt

COPY thermometer.py /app/thermometer.py



CMD ["python3", "/app/thermometer.py"]