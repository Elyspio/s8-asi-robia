import os
import random
import time

import requests

room = os.getenv("ROOM", "unspecified")
hud_host = os.getenv("HUD_HOST", "http://127.0.0.1:5000")


class Thermometer:

    def __init__(self):
        self.temperature = 10

    def send_data(self):
        requests.post(f"{hud_host}/sensors/thermometers/{room}", json={"value": self.temperature})
        self.temperature = random.randint(-10, 40)


if __name__ == '__main__':
    sensor = Thermometer()
    while True:
        sensor.send_data()
        time.sleep(1)
